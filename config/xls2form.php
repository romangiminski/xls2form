<?php

return [
    /**
     * Ścieżka do katalogu z plikiem.
     * Jeżeli zaczyna się od '/' - ścieżka bezwzględna.
     * Jeżeli nie zaczyna się od '/' - podkatalog w public.
     */
    'path' => env('XLS2FORM_PATH', null),

    /**
     * Nazwa pliku Excela (XLS).
     */
    'filename' => env('XLS2FORM_FILENAME', null),

    /**
     * Czas w minutach przechowywania danych w pamięci cache.
     *
     * null - wyłacza cache
     */
    'cache_lifetime' => env('XLS2FORM_CACHE_LIFETIME', null),

    /**
     * Prefix etykiety imortowanych kolumn.
     */
    'field_name_prefix' => env('XLS2FORM_FIELD_NAME_PREFIX', null),

    /**
     * Separator wartości dla pól z wieloma wartościami
     */
    'field_content_separator' => env('XLS2FORM_FIELD_CONTENT_SEPARATOR', ','),
];
