# Demo xls2form

1. Konfiguracja - wszystkie parametry w pliku .env, prefix XLS2FORM_:

   - **path** - ścieżka do pliku źródłowego, bezwzględna musi zaczynać się od '/', względna wskazuje na lokalizację w katalogu 'public',
   - **filename** - nazwa pliku źródłowego (Excel),

   - **cache_lifetime** - jeżeli plik źródłowy nie będzie często zmieniany, to można włączyć przechowywanie wyników importu w pamięci cache, czas w minutach,
   - **field_name_prefix** - prefix etykiet (nazw) importowanych kolumn, wszystkie bez prefiksu są pomijane,
   - **field_content_separator** - seperator wartości dla pól z wieloma wartościami, domyślnie ','.

2. Do pracy wymagane:

   - maatwebsite/excel (import z pliku Excel)
   - moduł php-sqlite3
   - pusta baza danych /var/www/xls2form.sqlite (baza nie jest używana, pozwala tylko ominąć domyślną konfigurację Laravel)

3. **Braki**: nie było zbyt wiele czasu, a ja nie jestem specjalistą JavaScript, więc interakcja formularza może odrobinę szwankować, ale zakładam, że logika i funkcjonalność spełnia założenia przedstawione w opisie zadania.

