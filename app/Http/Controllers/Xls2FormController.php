<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\XlsFileService;

class Xls2FormController extends Controller
{
    /**
     * Dane z pliku Excel
     *
     * @var Illuminate\Support\Collection
     */
    protected $data;

    /**
     * Etykiety dla combobox - nazwy kolumn z pliku Excel
     *
     * @var array
     */
    protected $keys;

    /**
     * Prefix etykiet importowanych kolumn
     *
     * @var string
     */
    protected $prefix;

    /**
     * Pobranie danych z pliku Excel
     *
     * @param \App\Services\XlsFileService $xls_file
     */
    public function __construct(XlsFileService $xls_file)
    {
        $this->data = $xls_file->data;
        $this->keys = array_keys($xls_file->data->first());
        $this->prefix = $xls_file->prefix;
    }

    /**
     * Pokaż stronę formularza.
     * Wyświetla combo dla pierwszej kolumny.
     *
     * @return void
     */
    public function show()
    {
        $combobox = $this->toCombo($this->keys[0]);
        return view('xls2form', [
            'field' => $combobox,
        ]);
    }

    /**
     * Ajax
     * Pobiera z kolekcji dane kolejnej kolumy.
     *
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function form(Request $request)
    {
        if (!$request->ajax()) {
            abort(503);
        }

        $fields = array_filter(
            $request->input(),
            function ($key) {
                return preg_match('/^' . $this->prefix . '/', $key);
            },
            ARRAY_FILTER_USE_KEY
        );

        $response['combo'] = $this->toCombo($fields);

        return $response;
    }

    /**
     * Tworzy snippet html z nowym combo.
     * Po ostatnim zwraca download button.
     *
     * @param mixed $search
     *
     * @return string
     */
    private function toCombo($search)
    {
        $vals = array();
        $combo = array();

        if (!is_array($search)) {
            $vals = $this->data->groupBy($search)->keys()->toArray();
            $combo['name'] = $search;
            $combo['key'] = $search;
            $remove = false;
        } else {
            $fields = array_values(array_diff($this->keys, array_keys($search)));
            if (!empty($fields)) {
                $next = $fields[0];
                $data = $this->data;
                foreach ($search as $key => $value) {
                    $data = $data->where($key, $value);
                }
                $vals = $data->groupBy($next)->keys()->toArray();
                $combo['name'] = $next;
                $combo['key'] = $next;
                if (count($vals) > 1) {
                    $remove = true;
                } else {
                    $remove = false;
                }
            } else {
                $finish = true;
            }
        }
        $combo['items'] = array_unique($vals);

        return view('combobox', [
            'combo' => (object) $combo ?? null,
            'remove' => $remove ?? null,
            'prefix' => $this->prefix,
            'finish' => $finish ?? null,
        ])->render();
    }
}
