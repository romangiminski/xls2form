<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Exceptions\FileNotFound;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $exception) {
            //
        });

        $this->reportable(function (FileNotFound $exception) {
            abort(500, 'File not found (deleted?) or bad file path: ' . $exception->getMessage());
        });

        $this->reportable(function (FileIsEmpty $exception) {
            abort(500, 'No data in file: ' . $exception->getMessage());
        });
    }
}
