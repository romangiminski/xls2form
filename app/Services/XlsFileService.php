<?php

namespace App\Services;

use App\Imports\XlsImport;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use App\Exceptions\FileNotFound;
use App\Exceptions\FileIsEmpty;

class XlsFileService
{
    /**
     * Ścieżka do katalogu - env('XLS2FORM_PATH')
     *
     * @var string
     */
    private $path;

    /**
     * Nazwa pliku XLS - env('XLS2FORM_FILENAME')
     *
     * @var string
     */
    private $filename;

    /**
     * Nazwa obiektu przechowywanego w cache.
     *
     * md5(filepath)
     *
     * @var string
     */
    private $cache_key;

    /**
     * Czas w minutach przechowywania w cache.
     *
     * null - wyłącza cache.
     *
     * @var int
     */
    private $cache_lifetime;

    /**
     * Pełna ścieżka do pliku
     *
     * @var string
     */
    private $filepath;

    /**
     * Separator wartości w polach wielowartościowych.
     *
     * @var string
     */
    private $separator;

    /**
     * Kolekcja danych z pliku XLS
     *
     * @var Illuminate\Support\Collection
     */
    public $data;

    /**
     * Prefix etykiet importowanych kolumn.
     *
     * @var string
     */
    public $prefix;

    /**
     * Inicjuje właściwości na podstawie konfiguracji, uruchamia import.
     */
    public function __construct()
    {
        $this->prefix = config('xls2form.field_name_prefix');
        $this->separator = config('xls2form.field_content_separator');
        $this->cache_lifetime = config('xls2form.cache_lifetime');
        $this->path = rtrim(config('xls2form.path'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->filename = config('xls2form.filename');

        $this->filepath = $this->path . $this->filename;
        if ($this->path[0] !== DIRECTORY_SEPARATOR) {
            $this->filepath = public_path($this->filepath);
        }
        $this->cache_key = md5($this->filepath);

        $this->data = $this->get();
    }

    /**
     * Pobieranie zawartości z pliku Excel
     *
     * @return object
     */
    private function get()
    {
        if (!$this->isCached()) {
            if (!is_readable($this->filepath)) {
                throw new FileNotFound($this->filepath);
            }

            $import = Excel::toCollection(new XlsImport(), $this->filepath)->first();
            if ($import->isEmpty()) {
                throw new FileIsEmpty($this->filepath);
            }

            $data = $this->prepare($import);
            if ($data->isEmpty()) {
                throw new FileIsEmpty($this->filepath);
            }

            if ($this->cache_lifetime) {
                \Cache::put($this->cache_key, $data, now()->addMinutes($this->cache_lifetime));
            }
        } else {
            $data = \Cache::get($this->cache_key);
        }

        return $data;
    }

    /**
     * Czyszczenie kolekcji z danych niepożądanych
     *
     * @param \Illuminate\Support\Collection $import
     *
     * @return \Illuminate\Support\Collection $prepared
     */
    private function prepare(Collection $import)
    {
        $prefix = \Str::upper($this->prefix);
        $separator = $this->separator;

        $prepared = collect();

        // Pomiń kolumny z etykietą niezgodą z $prefix
        foreach ($import as $item) {
            $prepared->push($item->filter(function ($value, $key) use ($prefix) {
                return \Str::startsWith(\Str::upper($key), $prefix) && !\Str::endsWith(\Str::upper($key), $prefix);
            }));
        }

        // Usuń puste wiersze
        foreach ($prepared as $key => $item) {
            if ($item->values()->countBy()->count() === 1) {
                $prepared->forget($key);
            }
        }

        // Dodaj wiersze wg pól z kilkoma wartościami
        $prepared = $this->convertMultivalueFields($prepared, $separator);

        // Konwersja do "zwykłej" kolekcji
        $prepared = collect($prepared->toArray());

        return $prepared;
    }

    /**
     * Sprawdza obecność w cache
     *
     * @return boolean
     */
    private function isCached()
    {
        if ($this->cache_lifetime) {
            return \Cache::has($this->cache_key);
        } else {
            return false;
        }
    }

    /**
     * Rekurencyjnie przeszukuje kolekcję w poszukiwaniu pól z kilkoma wartościami.
     * Dodaje nowe rekordy i redukuje pola do pojedynczych wartości.
     *
     * @param \Illuminate\Support\Collection $collection
     * @param string $separator
     *
     * @return Illuminate\Support\Collection $collection
     */
    private function convertMultivalueFields(Collection $collection, $separator = ',')
    {
        foreach ($collection as $key => $item) {
            $multi = $item->filter(function ($value, $key) use ($separator) {
                return preg_match("/$separator/", $value);
            });
            if (!$multi->isEmpty()) {
                foreach ($multi as $key => $value) {
                    $values = explode($separator, $value);
                    foreach ($values as $value) {
                        $item[$key] = trim($value);
                        $new = collect($item->toArray());
                        $collection->push($new);
                        $collection = $collection->values();
                    }
                }
                $collection = $this->convertMultivalueFields($collection, $separator);
            }
        }

        return $collection;
    }
}
