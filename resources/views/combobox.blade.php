@if (!isset($finish))
<div class="grid grid-cols-1 md:grid-cols-2 form-group field" id="{{ $combo->name }}">
    <div class="p-2">
        <div class="flex items-center">
            <select name="{{ $combo->name }}" class="form-control">
                <option value="null" disabled selected>{{ \Str::upper(\Str::replaceFirst($prefix, '', $combo->name)) }}</option>
                @foreach ($combo->items as $key => $item)
                <option value="{{ $item }}">{{ empty($item) ? 'Empty' : $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="p-2">
        @if ($remove)
        <button type="button" class="btn btn-danger remove hidden" data-remove="{{ $combo->name }}">remove</button>
        @endif
    </div>
</div>
@else
<div class="grid grid-cols-1 md:grid-cols-1 p-4 field">
    <button type="button" class="btn btn-success download">DOWNLOAD</button>
</div>
@endif
