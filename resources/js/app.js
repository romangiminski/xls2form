// require('./bootstrap');

$(document).ready(function () {
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $('#fields').on('change', 'select', function(event) {
    event.preventDefault();

    $(this).closest('.field').find('button').show();
    $(this).parent().parent().parent().nextAll('.field').remove();

    var $serializedData = $('#xls2form').serialize();

    request = $.ajax({
      url: '/',
      type: 'POST',
      data: $serializedData
    });

    request.done(function (response, textStatus, jqXHR){
      $('#fields').append(response.combo);
    });

    request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

    request.always(function () {
      //
    });

  });

  $('#fields').on('click', 'button.remove', function () {
    $(this).parent().parent().nextAll('.field').remove();
    var remove=$(this).data('remove');
    $('div#'+remove).find('select')[0].selectedIndex = 0;
    $('div#'+remove).find('button').hide();
    var $s = $('#xls2form').find('select').length;
    if ($s === 1) {
      location.reload();
    }
  });

  $('#fields').on('click', 'button.download', function (event) {
    event.preventDefault();
    alert("The file is being downloaded");
  })
});

