<?php

use App\Http\Controllers\Xls2FormController;
use Illuminate\Support\Facades\Route;
use Illuminate\Mail\Markdown;

Route::get('/', [Xls2FormController::class, 'show'])->name('index');
Route::post('/', [Xls2FormController::class, 'form'])->name('form');

Route::get('/readme', function () {
    $slot = file_get_contents('README.md');
    return Markdown::parse($slot);
});
